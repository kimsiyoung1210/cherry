using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeBnt : MonoBehaviour
{
    public string upgradeName;
    public int goldByUpgrade;
    public int startGoldByUpgrade = 1;
    public int curCost = 1;
    public int startCurCost = 1;
    public int level = 1;
    public float upgradePow = 1.07f;
    public float costPow = 3.14f;
    public Text upgradeLevel;
    public Text upgradeCost;
    
    void Start()
    {
        DataController.GetInstance().LoadUpgradeBnt(this);
        UpdateUI();
    }

    public void PurchaseUpgrade()
    {
        if(DataController.GetInstance().GetGold()>= curCost)
        {
            DataController.GetInstance().SubGold(curCost);
            level++;
            DataController.GetInstance().AddGoldClick(goldByUpgrade);
            UpdateUpgrade();
            UpdateUI();
            Debug.Log(curCost);
            Debug.Log(level);
            DataController.GetInstance().SaveUpgradeBnt(this);
        }
    }
    public void UpdateUpgrade()
    {
        goldByUpgrade = startGoldByUpgrade * (int)Mathf.Pow(upgradePow, level);
        curCost = startCurCost * (int)Mathf.Pow(costPow, level);
    }
    public void UpdateUI()
    {
        upgradeLevel.text = "Level." + level;
        upgradeCost.text =  curCost.ToString();
    }
    void Update()
    {
        
    }
}
