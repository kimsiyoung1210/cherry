using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Text goldText;
    public Text goldClickText;
    public DataController dataController;
    public GameObject menuPanel;
    void Start()
    {
        
    }

    
    void Update()
    {
        goldText.text = "GOLD: " + dataController.GetGold();
        goldClickText.text = "OneClick " + dataController.GetGoldClick(); 
    }

    public void MenuOffBnt()
    {
        menuPanel.SetActive(false);
    }

    public void MenuOnBnt()
    {
        menuPanel.SetActive(true);
    }
}
