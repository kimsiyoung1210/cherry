using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemData : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LoadItemBnt(ItemScript itemScript)
    {
        string item = itemScript.itemName;
        itemScript.level = PlayerPrefs.GetInt(item + "_level");
        //Debug.Log(PlayerPrefs.GetInt(key + "_level"));
        itemScript.curCost = PlayerPrefs.GetInt(item + "_cost", itemScript.startCurCost);
        //Debug.Log(PlayerPrefs.GetInt(key + "_cost"));
        itemScript.goldSec = PlayerPrefs.GetInt(item + "_goldSec");
        //Debug.Log(PlayerPrefs.GetInt(key + "_goldSec"));
        itemScript.firstClick = PlayerPrefs.GetInt(item + "_firstClick");
        

        if (PlayerPrefs.GetInt(item + "_isOpen") == 1)
        {
            itemScript.isOpen = true; 
        }
        else
        {
            itemScript.isOpen = false;
        }

        if(itemScript.isOpen == true)
        {
            itemScript.itemPoint.GetComponent<Item_SaengSung>().ItemSS();
        }

    }

    public void SaveItemBnt(ItemScript itemScript)
    {
        string item = itemScript.itemName;
        PlayerPrefs.SetInt(item + "_level", itemScript.level);
        PlayerPrefs.SetInt(item + "_cost", itemScript.curCost);
        PlayerPrefs.SetInt(item + "_goldSec", itemScript.goldSec);
        PlayerPrefs.SetInt(item + "_firstClick", itemScript.firstClick);

        if (itemScript.isOpen == true)
        {
            PlayerPrefs.SetInt(item + "_isOpen", 1);
        }
        else
        {
            PlayerPrefs.SetInt(item + "_isOpen", 0);
        }
    }

}
