using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mystaus
{
    public int level;
    public string playerName;
    public int maxHp;
    public int maxMp;
    public Vector3 lastPos;
}
public class Example_json : MonoBehaviour
{
   
    void Start()
    {
        Mystaus mystaus = new Mystaus();
        mystaus.level = 1;
        mystaus.playerName = "Cherry";
        mystaus.maxHp = 100;
        mystaus.maxMp = 50;
        mystaus.lastPos = new Vector3(1, 1, 1);

        string jsonData = JsonUtility.ToJson(mystaus);
        //Debug.Log(jsonData);
        PlayerPrefs.SetString("PlayerData", jsonData);
        PlayerPrefs.GetString("PlayerData");
        //Debug.Log(PlayerPrefs.GetString("PlayerData"));
        string getedString = PlayerPrefs.GetString("PlayerData");

        Mystaus getedStaus = JsonUtility.FromJson<Mystaus>(getedString);
        Debug.Log("레벨   " + getedStaus.level);
        Debug.Log("이름   " + getedStaus.playerName);
        Debug.Log("Hp   " + getedStaus.maxHp);
        Debug.Log("Mp   " + getedStaus.maxMp);
        Debug.Log("마지막 위치   " +getedStaus.lastPos);
    }


    void Update()
    {
        
    }
}
