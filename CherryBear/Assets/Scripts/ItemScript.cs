using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemScript : MonoBehaviour
{
    public Text itemMyeong;
    public Text itemLevel;
    public Text itemCost;
    public Text itemGoldSec;
    public string itemName;
    public int level;
    public int curCost;
    public int startCurCost = 1;
    public int goldSec;
    public int startGoldSec = 1;
    public float costPow = 3.14f;
    public float upgradePow = 1.07f;
    public int firstClick = 0;
    public GameObject itemPoint;
    public ItemData itemData;


    public bool isOpen = false;
    void Start()
    {
        itemData.LoadItemBnt(this);
        StartCoroutine("AddGoldLoop");
        UpdateUI();
    }

    
    void Update()
    {
        
    }

    public void PurChaseItem()
    {
        if(DataController.GetInstance().GetGold()>= curCost)
        {
            isOpen = true;
            DataController.GetInstance().SubGold(curCost);
            level++;
            firstClick++;
            UpdateItem();
            UpdateUI();
            //Debug.Log(curCost);
            //Debug.Log(level);
            itemData.SaveItemBnt(this);

            if(firstClick == 1)
            {
                itemPoint.GetComponent<Item_SaengSung>().ItemSS();
            }
            
        }
    }

    IEnumerator AddGoldLoop()
    {
        while (true)
        {
            if (isOpen)
            {
                DataController.GetInstance().AddGold(goldSec);
            }

            yield return new WaitForSeconds(1.0f);
        }
    }

    public void UpdateItem()
    {
        goldSec += startGoldSec * (int)Mathf.Pow(upgradePow, level);
        curCost = startCurCost * (int)Mathf.Pow(costPow, level);
    }

    public void UpdateUI()
    {
        itemMyeong.text = itemName;
        itemLevel.text = "Level." + level;
        itemCost.text = curCost.ToString();
        itemGoldSec.text = "�� ��: " + goldSec;
    }
}
