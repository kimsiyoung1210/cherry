using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataController : MonoBehaviour
{
    private static DataController instance;
    public static DataController GetInstance()
    {
        if (instance == null)
        {
            instance = FindObjectOfType<DataController>();
            if(instance == null)
            {
                GameObject container = new GameObject("DataController");
               instance = container.AddComponent<DataController>();
            }
        }
        return instance;
    }

    private int m_gold = 0;
    private int m_goldClick = 0;

    private void Awake()
    {
        m_gold = PlayerPrefs.GetInt("Gold");
        m_goldClick = PlayerPrefs.GetInt("GoldClick", 1);
    }

    public void SetGold(int newGold)
    {
        m_gold = newGold;
        PlayerPrefs.SetInt("Gold", m_gold);
    }

    public void AddGold(int newGold)
    {
        m_gold += newGold;
        SetGold(m_gold);
    }

    public void SubGold(int newGold)
    {
        m_gold -= newGold;
        SetGold(m_gold);
    }

    public int GetGold()
    {
        return m_gold;
    }

    public int GetGoldClick()
    {
        return m_goldClick;
    }

    public void SetGoldClick(int newGoldClick)
    {
        m_goldClick = newGoldClick;
        PlayerPrefs.SetInt("GoldClick", m_goldClick);
    }

    public void AddGoldClick(int newGoldClick)
    {
        m_goldClick += newGoldClick;
        SetGoldClick(m_goldClick);
    }

    public void LoadUpgradeBnt(UpgradeBnt upgradeBnt)
    {
        string key = upgradeBnt.upgradeName;
        upgradeBnt.level = PlayerPrefs.GetInt(key + "_level", 1);
        //Debug.Log(PlayerPrefs.GetInt(key + "_level"));
        upgradeBnt.goldByUpgrade = PlayerPrefs.GetInt(key + "_goldByUpgrade", upgradeBnt.startGoldByUpgrade);
        //Debug.Log(PlayerPrefs.GetInt(key + "_goldByUpgrade"));
        upgradeBnt.curCost = PlayerPrefs.GetInt(key + "_cost", upgradeBnt.startCurCost);
        //Debug.Log(PlayerPrefs.GetInt(key + "_cost"));
    }

    public void SaveUpgradeBnt(UpgradeBnt upgradeBnt)
    {
        string key = upgradeBnt.upgradeName;
        PlayerPrefs.SetInt(key + "_level", upgradeBnt.level);
        PlayerPrefs.SetInt(key + "_goldByUpgrade", upgradeBnt.goldByUpgrade);
        PlayerPrefs.SetInt(key + "_cost", upgradeBnt.curCost);
    }

   

}
