using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MainCha : MonoBehaviour
{
    public Transform[] points;
    public Animator animator;
    public bool isWalk = false;
    public float curTime;
    public float coolTime;
    public NavMeshAgent navMesh;
    public enum MAINSTATE
    {
        IDLE = 0,
        WALK
    }

    public MAINSTATE main;
    void Start()
    {
        animator = GetComponent<Animator>();
        main = MAINSTATE.IDLE;
    }

    // Update is called once per frame
    void Update()
    {
        switch (main)
        {
            case MAINSTATE.IDLE:
                isWalk = false;
                curTime += Time.deltaTime;
                if(curTime > coolTime)
                {
                    int rnd = Random.Range(0, 2);
                    if(rnd == 0)
                    {
                        main = MAINSTATE.IDLE;
                    }
                    else
                    {
                        main = MAINSTATE.WALK;
                    }
                    curTime = 0;
                }
                break;
            case MAINSTATE.WALK:
                curTime += Time.deltaTime;
                if(curTime > 3f)
                {
                    int rnd = Random.Range(0, 2);
                    if(rnd == 0)
                    {
                        main = MAINSTATE.IDLE;
                    }
                    else
                    {
                        GoToPoint();
                        isWalk = true;
                    }
                    curTime = 0;
                }
                break;
            default:
                break;
        }
        animator.SetBool("WALK", isWalk);
    }

    public void GoToPoint()
    {
        navMesh.SetDestination(points[Random.Range(0, points.Length)].transform.position);
    }
}
